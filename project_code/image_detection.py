from imageai.Detection import ObjectDetection
import os

my_path = os.getcwd()
output_path = "C:/Users/Mohammad/Desktop/object_detection/objectsdetected.jpg"
detector = ObjectDetection()
input_path = "5.jpg"
model_path = "C:/Users/Mohammad/Desktop/object_detection/models/resnet50_coco_best_v2.0.1.h5"
detector.setModelTypeAsRetinaNet()
detector.setModelPath(os.path.join(my_path, model_path))
detector.loadModel()
detections = detector.detectObjectsFromImage(input_image=os.path.join(my_path, input_path),
                                             output_image_path=os.path.join(my_path, output_path))
# for eachobj in detections:
#   print(eachobj["name"] + " : " + eachobj["percentage_probability"] )

for eachItem in detections:
    print(eachItem["name"], " : ", eachItem["percentage_probability"])

detections, extracted_images = detector.detectObjectsFromImage(input_image=os.path.join(my_path, input_path),
                                                               output_image_path=os.path.join(my_path, output_path),
                                                               extract_detected_objects=True)